package com.pkapps.dotainfo.Fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkapps.dotainfo.Activities.MatchAnalysisActivity;
import com.pkapps.dotainfo.Adapters.MainList;
import com.pkapps.dotainfo.AsyncCalls.AllMatches;
import com.pkapps.dotainfo.AsyncCalls.HeroStats;
import com.pkapps.dotainfo.AsyncCalls.MatchDetails;
import com.pkapps.dotainfo.AsyncCalls.PlayerProfile;
import com.pkapps.dotainfo.AsyncCalls.Totals;
import com.pkapps.dotainfo.CacheDB.AllMatchesTable;
import com.pkapps.dotainfo.CacheDB.AppDatabase;
import com.pkapps.dotainfo.CacheDB.OverviewTable;
import com.pkapps.dotainfo.R;

import java.util.List;


public class MatchListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View parent_view;
    private Context ctx;
    private RecyclerView recyclerView;
    public SwipeRefreshLayout refresher;
    private MainList mAdapter;
    private ImageView imgVw;
    private TextView personaname;
    public ProgressDialog p;
    private Toolbar toolbar;
    List<AllMatchesTable> items;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    boolean isMatchClicked;
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.content_main_navigation_drawer,container,false);
        ctx = getActivity();
        pref = ctx.getSharedPreferences("prefs",Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString("login","true");
        editor.commit();
        isMatchClicked = false;
        initComponent();
        return view;
    }
    private void initComponent() {

        refresher = (SwipeRefreshLayout) view.findViewById(R.id.refresherView);
        refresher.setOnRefreshListener(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        recyclerView.setHasFixedSize(true);
        items = AppDatabase.getAppDatabase(ctx).getAllMatchesDao().getLimitedMatches(50);
        //set data and list adapter
        mAdapter = new MainList(ctx, items);
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new MainList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, AllMatchesTable obj, int position) {
                //Snackbar.make(parent_view, "Item " + obj.getMatchID() + " clicked", Snackbar.LENGTH_SHORT).show();
                if(isMatchClicked!=true) {
                    isMatchClicked = true;
                    editor.putLong("matchId", obj.getMatchID());
                    editor.commit();
                    OverviewTable matchData = AppDatabase.getAppDatabase(ctx).getOverviewDao().getMatchOverviewData(obj.getMatchID());
                    if(matchData==null) {
                        new MatchDetails(ctx, obj.getMatchID()).execute();
                    }else{
                        Intent intent = new Intent(ctx, MatchAnalysisActivity.class);
                        intent.putExtra("matchId",obj.getMatchID());
                        ctx.startActivity(intent);
                    }
                }
            }
        });
    }
    @Override
    public void onRefresh() {
        String id32 = pref.getString("steam32",null);
        new PlayerProfile(ctx,id32).execute();
        new AllMatches(ctx,id32).execute();
        new Totals(ctx,id32).execute();
        new HeroStats(ctx,id32).execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        isMatchClicked = false;
    }
}
