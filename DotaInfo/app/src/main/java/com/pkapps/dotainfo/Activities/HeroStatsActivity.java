package com.pkapps.dotainfo.Activities;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

import com.pkapps.dotainfo.Adapters.HeroStatList;
import com.pkapps.dotainfo.Adapters.MainList;
import com.pkapps.dotainfo.AsyncCalls.HeroStats;
import com.pkapps.dotainfo.CacheDB.AppDatabase;
import com.pkapps.dotainfo.CacheDB.HeroStatsTable;
import com.pkapps.dotainfo.R;

import java.util.List;

public class HeroStatsActivity extends Activity {

    private RecyclerView recyclerView;
    private Context ctx;
    private SwipeRefreshLayout refresher;
    List<HeroStatsTable> items;
    private HeroStatList mAdapter;
    ImageButton back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hero_stats);
        back = (ImageButton)findViewById(R.id.backButton);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        initComponent();
    }

    public void initComponent(){
        recyclerView = (RecyclerView) findViewById(R.id.heroRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        setAdapter();
    }
    public void setAdapter(){
        items = AppDatabase.getAppDatabase(ctx).getHeroStatsDao().getStats();
        mAdapter = new HeroStatList(this,items);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
