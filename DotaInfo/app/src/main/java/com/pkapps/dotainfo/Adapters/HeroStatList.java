package com.pkapps.dotainfo.Adapters;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pkapps.dotainfo.Activities.MatchAnalysisActivity;
import com.pkapps.dotainfo.AsyncCalls.MatchDetails;
import com.pkapps.dotainfo.CacheDB.AllMatchesTable;
import com.pkapps.dotainfo.CacheDB.AppDatabase;
import com.pkapps.dotainfo.CacheDB.HeroStatsTable;
import com.pkapps.dotainfo.CacheDB.OverviewTable;
import com.pkapps.dotainfo.Constants.DotaMisc;
import com.pkapps.dotainfo.Fragments.HeroFilterFragment;
import com.pkapps.dotainfo.R;
import com.pkapps.dotainfo.Tools.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PraveenKumar on 4/3/2018.
 */

public class HeroStatList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context ctx;
    List<HeroStatsTable> items;
    private MainList mAdapter;

    public HeroStatList(Context ctx, List<HeroStatsTable> items) {
        this.ctx = ctx;
        this.items = items;
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView name;
        public TextView won;
        public TextView total;
        public TextView recent;
        public View lyt_parent;


        public OriginalViewHolder(View v) {
            super(v);
            image = (ImageView) v.findViewById(R.id.image);
            won = (TextView) v.findViewById(R.id.won);
            total = (TextView) v.findViewById(R.id.total);
            name = (TextView) v.findViewById(R.id.name);
            lyt_parent = (View) v.findViewById(R.id.lyt_parent);
            recent = (TextView) v.findViewById(R.id.recent);
        }
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.hero_stat_list_layout
                , parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof OriginalViewHolder) {
            OriginalViewHolder view = (OriginalViewHolder) holder;
            float percent;

            final HeroStatsTable p = items.get(position);
            view.name.setText(DotaMisc.getHeroName(p.getHeroID()));
            view.image.setImageResource(ctx.getResources().obtainTypedArray(R.array.hero_images).getResourceId(p.getHeroID()-1,114));
            view.won.setText(Integer.toString(p.getWon()));
            view.total.setText(Integer.toString(p.getGamesPLayed()));
            view.recent.setText(Util.getDateCurrentTimeZone(p.getMatchID()));

            view.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentTransaction ft = ((Activity)ctx).getFragmentManager().beginTransaction();
                    ft.addToBackStack(null);
                    HeroFilterFragment frag = new HeroFilterFragment();
                    Bundle b = new Bundle();
                    b.putInt("heroID",p.getHeroID());
                    frag.setArguments(b);
                    frag.show(ft,"filteredhero");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
