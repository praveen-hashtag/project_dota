package com.pkapps.dotainfo.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pkapps.dotainfo.Adapters.HeroesList;
import com.pkapps.dotainfo.R;

import java.util.List;

public class HerosFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    HeroesList mAdapter;
    List<HeroesList> items;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_main_heroes_drawer,container,false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mAdapter = new HeroesList();
        return view;
    }
}
