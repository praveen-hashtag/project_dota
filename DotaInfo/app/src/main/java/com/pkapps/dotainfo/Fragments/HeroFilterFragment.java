package com.pkapps.dotainfo.Fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.pkapps.dotainfo.Activities.MatchAnalysisActivity;
import com.pkapps.dotainfo.Adapters.MainList;
import com.pkapps.dotainfo.AsyncCalls.MatchDetails;
import com.pkapps.dotainfo.CacheDB.AllMatchesTable;
import com.pkapps.dotainfo.CacheDB.AppDatabase;
import com.pkapps.dotainfo.CacheDB.OverviewTable;
import com.pkapps.dotainfo.R;

import java.util.List;

public class HeroFilterFragment extends DialogFragment {
    private MainList mAdapter;
    private Context ctx;
    int heroID;
    boolean isMatchClicked = false;
    private RecyclerView recyclerView;
    private ImageButton back;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if(d!=null){
            d.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.hero_filter_fragment,container,false);
        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        back = (ImageButton) root.findViewById(R.id.back);
        heroID = getArguments().getInt("heroID");
        ctx = getActivity();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });
        List<AllMatchesTable> items = AppDatabase.getAppDatabase(ctx).getAllMatchesDao().getMatchesOnHero_ID(heroID);
        mAdapter = new MainList(ctx,items);
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new MainList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, AllMatchesTable obj, int position) {
                //Snackbar.make(parent_view, "Item " + obj.getMatchID() + " clicked", Snackbar.LENGTH_SHORT).show();
                if(isMatchClicked!=true) {
                    isMatchClicked = true;
                    pref = ctx.getSharedPreferences("prefs",Context.MODE_PRIVATE);
                    editor = pref.edit();
                    editor.putLong("matchId", obj.getMatchID());
                    editor.commit();
                    OverviewTable matchData = AppDatabase.getAppDatabase(ctx).getOverviewDao().getMatchOverviewData(obj.getMatchID());
                    if(matchData==null) {
                        new MatchDetails(ctx, obj.getMatchID()).execute();
                    }else{
                        Intent intent = new Intent(ctx, MatchAnalysisActivity.class);
                        intent.putExtra("matchId",obj.getMatchID());
                        ctx.startActivity(intent);
                    }
                }
            }
        });
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        isMatchClicked = false;
    }
}
